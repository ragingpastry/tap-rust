use clap::Parser;
use gitlab::Gitlab;

mod helpers;

#[derive(Parser)]
#[clap(author, version, trailing_var_arg = true)]
struct Args {
    /// The project ID to look for
    #[clap(short, long)]
    project: Option<String>,
    /// Gitlab API key (Must have API read/write)
    #[clap(short, long)]
    api_key: Option<String>,
    #[clap(short, long, action)]
    json: bool,
    variables: Option<Vec<String>>,
}

fn main() {
    let args = Args::parse();

    let git_folder = helpers::find_git_folder().unwrap();
    let remote = helpers::parse_remote(&git_folder);
    let head = helpers::parse_head(&git_folder).unwrap();

    let project: String = match args.project.as_deref() {
        Some(project) => String::from(project),
        None => {
            let project = &remote.path;
            String::from(project)
        }
    };

    let api_key: String = match args.api_key.as_deref() {
        Some(api_key) => String::from(api_key),
        None => {
            let credentials = helpers::get_credentials(&remote).unwrap();
            String::from(credentials)
        }
    };

    match args.variables.as_deref() {
        Some(variables) => {
            for variable in variables {
                println!("{variable}")
            }
        }
        None => {
            println!("No variables received!")
        }
    };

    let remote_url = &remote.host.unwrap();

    let client = Gitlab::new(remote_url, api_key).unwrap();

    let created_pipeline: gitlab::Pipeline =
        helpers::create_pipeline(&client, head, project, args.variables);

    match args.json {
        false => {
            println!("ID: {:?}", created_pipeline.id);
            println!("Status: {:?}", created_pipeline.status);
            println!("URL: {:?}", created_pipeline.web_url);
        }
        true => {
            println!(
                "{}",
                serde_json::to_string_pretty(&created_pipeline).unwrap()
            );
        }
    }
}
