use git_url_parse::GitUrl;
use home::home_dir;
use std::ffi::OsStr;
use std::fs;
use std::fs::File;
use std::io::{BufRead, BufReader, Error};
use std::path::PathBuf;

use gitlab::api::{projects::pipelines, Query};
use gitlab::types::Pipeline;
use gitlab::Gitlab;
use url::Url;

fn folders(dir: &PathBuf) -> Result<Vec<PathBuf>, Error> {
    Ok(fs::read_dir(dir)?
        .into_iter()
        .filter(|r| r.is_ok()) // Get rid of Err variants for Result<DirEntry>
        .map(|r| r.unwrap().path()) // This is safe, since we only have the Ok variants
        .filter(|r| r.is_dir()) // Filter out non-folders
        .collect())
}

pub fn create_pipeline(
    client: &Gitlab,
    ref_: String,
    project: String,
    variables: Option<Vec<String>>,
) -> Pipeline {
    let mut variables_to_add = Vec::new();

    for variable in variables.unwrap_or(Vec::new()) {
        let key = String::from(variable.split("=").next().unwrap());
        let value = String::from(variable.split("=").last().unwrap());
        variables_to_add.push(
            pipelines::PipelineVariable::builder()
                .key(key)
                .value(value)
                .build()
                .unwrap(),
        );
    }

    let endpoint = pipelines::CreatePipeline::builder()
        .project(project)
        .ref_(ref_)
        .variables(variables_to_add.into_iter())
        .build()
        .unwrap();

    let pipeline: Pipeline = endpoint.query(client).unwrap();

    pipeline
}

pub fn parse_remote(gitconfig: &PathBuf) -> GitUrl {
    let data = std::fs::read_to_string(gitconfig.join("config")).expect("Unable to read file");
    let mut lines = data.lines();

    loop {
        let mut found = false;
        let remote = "[remote \"origin\"]";

        for line in &mut lines {
            if line.contains(&remote) {
                found = true;
                break;
            }
        }

        if !found {
            panic!("Did not find remote in file!")
        }

        for line in &mut lines {
            if line.contains("url") {
                let remote = line.split('=').last().unwrap().trim();
                return GitUrl::parse(&remote.to_owned()).unwrap();
            }
        }
    }
}

pub fn parse_head(gitconfig: &PathBuf) -> Result<String, String> {
    let data: String =
        std::fs::read_to_string(gitconfig.join("HEAD")).expect("Unable to read file");

    for line in data.lines() {
        if line.contains("ref") {
            let ref_ = line.split("/").last().unwrap().trim();
            return Ok(String::from(ref_));
        }
    }

    return Err("Could not find ref in .git/HEAD".to_string());
}

pub fn find_git_folder() -> Option<PathBuf> {
    let mut search_path = PathBuf::from(".");
    let mut git_folder: Option<PathBuf> = None;

    'outer: loop {
        let folder_list = folders(&search_path);

        match folder_list {
            Ok(v) => {
                for folder in v {
                    if folder.file_stem().unwrap() == OsStr::new(".git") {
                        git_folder = Some(folder.canonicalize().unwrap());
                        break 'outer;
                    }
                }
                search_path = search_path.join("../")
            }
            Err(e) => {
                println!("Could not parse folder path {e}");
                break 'outer;
            }
        }
    }

    git_folder
}

/// Need to fix error handling in here
pub fn get_credentials(remote: &GitUrl) -> Result<String, String> {
    let home = home_dir().unwrap();
    let credential_file = home.join(".git-credentials");
    let file = File::open(&credential_file).expect("Failed to find a .git-credentials file");

    let reader = BufReader::new(file);
    for line in reader.lines() {
        let entry = Url::parse(&line.expect("Could not read line")).expect("Failed to parse entry");
        if entry.host_str().unwrap() == remote.host.as_ref().unwrap().as_str() {
            return Ok(String::from(entry.password().unwrap()));
        }
    }
    return Err("Did not find remote in ($HOME/.git-credentials) file!".to_string());
}
